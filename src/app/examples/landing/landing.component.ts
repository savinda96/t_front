import { Component, OnInit, OnDestroy } from '@angular/core';
import * as Rellax from 'rellax';
import {AuthenticationService, UserService} from '../services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import {User} from '../user';
import {Patient} from '../user'


declare interface TableData {
  headerRow: string[];
  dataRows: string[][];
}
@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit, OnDestroy {
  courseForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  data: Date = new Date();
  pats: any[];
  injurys: any[];
  courses: any[];
  focus;
  focus1;
  public tableData1: TableData;
  currentUser: User;
  constructor(
      private authenticationService: AuthenticationService,
      private formBuilder: FormBuilder,
      private userService: UserService,
      private router: Router
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
  }

  ngOnInit() {
    this.courseForm = this.formBuilder.group({
      TreatmentName: ['', Validators.required],
      Movements: ['', Validators.required],
      Time: ['', Validators.required],
      TreatmentID: ['', Validators.required]
  });
    this.tableData1 = {
      headerRow: [ 'ID', 'Name', 'Country', 'City', 'Salary'],
      dataRows: [
          ['1', 'Dakota Rice', 'Niger', 'Oud-Turnhout', '$36,738'],
          ['2', 'Minerva Hooper', 'Curaçao', 'Sinaai-Waas', '$23,789'],
          ['3', 'Sage Rodriguez', 'Netherlands', 'Baileux', '$56,142'],
          ['4', 'Philip Chaney', 'Korea, South', 'Overland Park', '$38,735'],
          ['5', 'Doris Greene', 'Malawi', 'Feldkirchen in Kärnten', '$63,542'],
          ['6', 'Mason Porter', 'Chile', 'Gloucester', '$78,615']
      ]
  };
    const rellaxHeader = new Rellax('.rellax-header');
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('landing-page');
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');
    this.loadallpat();
    this.loadAlltreatments();
    this.loadAllcourses(this.currentUser._id);
  }

  get f() { return this.courseForm.controls; }

  ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('landing-page');
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.courseForm.invalid) {
        return;
    }

    this.loading = true;
    this.userService.regcourse(this.courseForm.value, this.currentUser._id)
    .subscribe(
        data => {
           this.loadAlltreatments();
        });
}

  logout = function() {
    this.authenticationService.logout();
    this.router.navigateByUrl('/index');
  }

  private loadallpat() {
    this.userService.getPatients(this.currentUser.username)
        .subscribe(data => this.pats = data);
}
private loadAlltreatments() {
  this.userService.getAllTreat()
      .subscribe(data => this.injurys = data);
}

viewme = function (id) {
  console.log(id);
};

approved = function (id) {
  this.userService.approved(id)
      .subscribe(() => this.loadallpat);
};

declined = function (id) {
  this.userService.declined()
  .subscribe(() => this.loadallpat);
};

private loadAllcourses(id) {
  this.userService.getAllcourse(id)
      .subscribe(data => this.courses = data);
}

deletecourse = function (id) {
  this.userService.deletecourse(id)
  .subscribe(() => this.loadAllcourses);
  this.router.navigateByUrl('/examples/landing');
};

navigateToPatientProgress(id: number) {
  this.router.navigate(['/patient-progress'], { queryParams: {Id: id}});
}

navigateToPatientChat(patientDetails: Patient) {
  this.router.navigate(['/chat'], { queryParams: {username: patientDetails.username}});
}

onChange(newValue) {
  const selectedInjury = this.injurys.filter((item) => item.TreatmentName === newValue);
  if (selectedInjury && selectedInjury[0]) {
    this.courseForm.controls.TreatmentID.setValue(selectedInjury[0]._id);
  }
}

}
