import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User, Re } from '../user';
import {Patient} from '../user';
import {Injury} from '../user';
import {Course} from '../user'
import { Ar } from '../user/ar';
import { Chat } from '../user/chat';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    register(user: User) {
        return this.http.post(`http://localhost:4000/users/tregister`, user);
    }
    regcourse(course: Course, id) {
        return this.http.post(`http://localhost:4000/users/regcourse/` + id, course);
    }
    getPatients(username) {
      console.log(username);
      return this.http.get<Patient[]>(`http://localhost:4000/users/getPatients/` + username);

    }
    getAllTreat() {
      return this.http.get<Injury[]>(`http://localhost:4000/users/treatment`);
  }
    approved(id){
      console.log(id)
     return this.http.patch<Patient>(`http://localhost:4000/users/approve/` + id, {'approved': 'true'});
    }
    declined(id){
      console.log(id)
     return this.http.delete<Patient>(`http://localhost:4000/users/declined/` + id);
    }

    getAllcourse(id) {
      console.log(id);
      return this.http.get<Course[]>(`http://localhost:4000/users/getCourses/` + id);
    }

    deletecourse(id) {
      console.log(id)
     return this.http.delete<Course>(`http://localhost:4000/users/deleteco/` + id);
    }

    getPatientDetails (id: string) {
      return this.http.get<Patient>(`http://localhost:4000/users/` + id);
    }

    getAchievedProgress(id: string) {
      return this.http.get<Ar[]>(`http://localhost:4000/users/ar/` + id);
    }

    getRequiredProgress(id: string) {
      return this.http.get<Re[]>(`http://localhost:4000/users/re/` + id);
    }

    getChat(patientusername: string, therapist: string) {
      return this.http.get<Chat[]>(`http://localhost:4000/users/chat`, {params: {patient: patientusername, therapist: therapist}});
    }

    sendChat(chat: any) {
      return this.http.post<any>(`http://localhost:4000/users/chat`, chat);
    }

    editRequiredProgress(id: string, requiredProgress: Re) {
      return this.http.post<any>(`http://localhost:4000/users/reprogress/` + id, requiredProgress);
    }
}
