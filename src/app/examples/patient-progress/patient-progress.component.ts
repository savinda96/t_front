import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../services';
import { ActivatedRoute, Router } from '@angular/router';
import { Patient, Ar, Re } from '../user';
import { ChartOptions } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
@Component({
  selector: 'app-patient-progress',
  templateUrl: './patient-progress.component.html',
  styleUrls: ['./patient-progress.component.scss']
})

export class PatientProgressComponent implements OnInit, OnDestroy {
  patientId: string;
  patientDetails: Patient;
  achievedResults: Ar[];
  requiredResults: Re[];
  isShowProgress = false;
  chartOptions: (ChartOptions & { annotation: any }) =  {
    responsive: true,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            max: 50
            }
        },
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'horizontal',
          scaleID: 'y-axis-0',
          value: '1',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'Required Movement'
          }
        },
        {
          type: 'line',
          mode: 'horizontal',
          scaleID: 'y-axis-0',
          value: '5',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'Required Time'
          }
        },
      ],
    },
  };
  lineChartPlugins = [pluginAnnotations];
  movementData:  Array<any> = [];
  temporalData: Array<any> = [];
  chartLabels = [];
  chartData = [];
  chartAnnotations = [];
  constructor(private http: UserService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
    this.route.queryParams.subscribe(params => {
      this.patientId = params['Id'];
      this.dataLoad();
    })
  }

  ngOnDestroy() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');
  }

  private async dataLoad() {
    this.patientDetails =  await this.getPatientDetails();
    this.achievedResults = await this.getAchievedProgress();
    this.requiredResults = await this.getRequiredProgress();
  }

  private async getPatientDetails() {
    return this.http.getPatientDetails(this.patientId).toPromise<Patient>();
  }

  private async getAchievedProgress() {
    return this.http.getAchievedProgress(this.patientId).toPromise<Ar[]>();
  }

  private async getRequiredProgress() {
    return this.http.getRequiredProgress(this.patientId).toPromise<Re[]>();
  }

  getChartDataForAchivedResults() {
    this.achievedResults.map( (achievedItem) => {
      this.movementData.push(+achievedItem.AchievedProgress[0].Movements);
      this.temporalData.push(+achievedItem.AchievedProgress[0].Time);
      this.chartLabels.push(achievedItem.Date);
    });
    this.chartData.push({data: this.movementData, label: 'Actual Movement'});
    this.chartData.push({data: this.temporalData, label: 'Actual Time'});
  }

  getChartDataForRequiredResults() {
    this.requiredResults.map( (item) => {
      this.chartOptions.annotation.annotations[0].value = +item.RequiredProgress[0].Movements;
      this.chartOptions.annotation.annotations[1].value = +item.RequiredProgress[0].Time;
    });
  }

  showProgress() {
    this.getChartDataForAchivedResults();
    this.getChartDataForRequiredResults();
    this.isShowProgress = true;
  }

  navigateToEditProgress() {
    this.router.navigate(['/edit-progress'],
         { queryParams: {patientId: this.patientId, requiredProgressId: this.requiredResults[0].id}});
  }

}
