import { Component, OnInit } from '@angular/core';
import { UserService, AuthenticationService } from '../services';
import { ActivatedRoute } from '@angular/router';
import { User } from '../user';
import { Chat } from '../user/chat';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  patientUserName: string;
  chat: Array<any> = [];
  currentUser: User;
  chatValue = '';
  constructor(private http: UserService,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService) {
      this.currentUser = this.authenticationService.currentUserValue;
     }

  ngOnInit() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
    this.route.queryParams.subscribe(params => {
      this.patientUserName = params['username'];
      this.getChat(this.patientUserName);
    })
  }

  getChat(username: string) {
    this.http.getChat(username, this.currentUser.username).subscribe( (data) => {
      this.chat = data.reverse();
    });
  }

  sendChat() {
    const chatMessage = {patient: this.patientUserName, therapist: this.currentUser.username,
       msg: this.chatValue, therapisttopatient: true};
    this.http.sendChat(chatMessage).subscribe(() => {
      this.chatValue = '';
      this.getChat(this.patientUserName);
    });
  }
}
