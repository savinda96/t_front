import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import {UserService} from '../services'

@Component({
  selector: 'app-signupTherapist',
  templateUrl: './signupTherapist.component.html',
  styleUrls: ['./signupTherapist.component.scss']
})
export class SignupTherapistComponent implements OnInit {

    data : Date = new Date();
    error:string;
    registerForm: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private userService: UserService) { }

    ngOnInit() {

        this.registerForm = this.formBuilder.group({
            fname: ['', [Validators.required,Validators.pattern('^[a-zA-Z\s]*$')]],
            lname: ['', [Validators.required,Validators.pattern('^[a-zA-Z\s]*$')]],
            gender: ['',Validators.required],
            contactNo: ['', [Validators.required]],
            username: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });

        


        var body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');

        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.add('navbar-transparent');
    }
    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            debugger
            return;
        }

        
        this.loading = true;
        

        this.userService.register(this.registerForm.value)
             .pipe(first())
             .subscribe(
                 data => {
                    this.router.navigate(['/index'], { queryParams: { registered: true }});
                 },
                 error => {
                     this.error=error.error.message;
                     console.log(error.message);
                     this.loading = false;
                 }); 

    }
    ngOnDestroy(){
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('login-page');

        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.remove('navbar-transparent');
    }

    btnClick= function () {
        this.router.navigateByUrl('/index');
    };

}
