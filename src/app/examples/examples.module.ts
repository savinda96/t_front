import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { AgmCoreModule } from '@agm/core';

import { LandingComponent } from './landing/landing.component';
import {SignupTherapistComponent} from './signupTherapist/signupTherapist.component';
import {UserService, AuthenticationService } from './services';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Jwtinterceptor } from './services';
import { ExamplesComponent } from './examples.component';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { PatientProgressComponent } from './patient-progress/patient-progress.component';
import { ChartsModule } from 'ng2-charts';
import 'chartjs-plugin-annotation/src/index.js';
import { ChatComponent } from './chat/chat.component';
import { EditProgressComponent } from './edit-progress/edit-progress.component';


@NgModule({
    imports: [
        CommonModule,
        NgCircleProgressModule.forRoot({
            // set defaults here
            radius: 80,
            outerStrokeWidth: 16,
            innerStrokeWidth: 8,
            outerStrokeColor: "#78C000",
            innerStrokeColor: "#C7E596",
            animationDuration: 300,
          }),
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        NouisliderModule,
        HttpClientModule,
        JwBootstrapSwitchNg2Module,
        AgmCoreModule.forRoot({
            apiKey: 'YOUR_KEY_HERE'
        }),
        ChartsModule
    ],
    declarations: [
        LandingComponent,
        LoginComponent,
        ExamplesComponent,
        SignupTherapistComponent,
        PatientProgressComponent,
        ChatComponent,
        EditProgressComponent,
    ],
    providers: [UserService,AuthenticationService,{provide: HTTP_INTERCEPTORS,
        useClass: Jwtinterceptor,
        multi: true}]
})
export class ExamplesModule { }
