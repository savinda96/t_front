import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService, AuthenticationService } from '../services';
import { ActivatedRoute } from '@angular/router';
import { User, Course, Re } from '../user';

@Component({
  selector: 'app-edit-progress',
  templateUrl: './edit-progress.component.html',
  styleUrls: ['./edit-progress.component.scss']
})
export class EditProgressComponent implements OnInit, OnDestroy {
  currentUser: User;
  allCourses: Course[] = [];
  patientId: string;
  requiredProgressId: string;
  requiredProgress: Re ;
  constructor(private http: UserService,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService) {
      this.currentUser = this.authenticationService.currentUserValue;
     }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.patientId = params['patientId'];
      this.requiredProgressId = params['requiredProgressId'];
      this.getRequiredProgress();
      this.getCourses(this.currentUser._id);
    })
  }

  onChange(newValue) {
   const course =  this.allCourses.filter( (item) => item._id === newValue);
   if (course && course[0]) {
       this.requiredProgress.RequiredProgress[0].Movements = course[0].Movements;
       this.requiredProgress.RequiredProgress[0].Time = course[0].Time;
       this.requiredProgress.TreatmentID =   course[0].TreatmentID;
   }
 }

  ngOnDestroy() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
  }

  getCourses(id: number) {
     this.http.getAllcourse(id).subscribe( (data) => {
        this.allCourses = data;
     });
  }

  saveRequiredProgress() {
      this.http.editRequiredProgress(this.requiredProgressId, this.requiredProgress).subscribe( (data) => {
          // toast
      });
  }

   getRequiredProgress() {
      this.http.getRequiredProgress(this.patientId).subscribe( (data) => {
         this.requiredProgress = data[0];
      });
  }

}
