export class Chat {
    id: number;
    patient: string;
    therapist: string;
    Date: Date;
    msg: string;
    therapisttopatient: boolean
}
