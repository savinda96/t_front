export class Course {
    _id: number;
    TreatmentName: string;
    Movements: string;
    Time:string;
    TreatmentID: string;
}