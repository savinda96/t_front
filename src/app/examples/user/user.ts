export class User {
    _id: number;
    fname: string;
    lname: string;
    gender:string;
    contactNo: string;
    username: string;
    password: string;
    token: string;
}