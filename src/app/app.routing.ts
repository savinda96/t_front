import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import {
    AuthGuardService as AuthGuard
  } from './examples/services';

import { ComponentsComponent } from './components/components.component';
import { LandingComponent } from './examples/landing/landing.component';
import { LoginComponent } from './examples/login/login.component';
import {SignupTherapistComponent} from './examples/signupTherapist/signupTherapist.component';
import {PatientProgressComponent} from './examples/patient-progress/patient-progress.component';
import {ChatComponent} from './examples/chat/chat.component';
import { NucleoiconsComponent } from './components/nucleoicons/nucleoicons.component';
import { from } from 'rxjs';
import { EditProgressComponent } from './examples/edit-progress/edit-progress.component';

const routes: Routes = [
    { path: '', redirectTo: 'index', pathMatch: 'full' },
    { path: 'index', component: LoginComponent },
    { path: 'nucleoicons', component: NucleoiconsComponent },
    { path: 'examples/landing', component: LandingComponent, canActivate: [AuthGuard] },
    { path: 'components', component: ComponentsComponent},
    { path: 'examples/signupTherapist', component: SignupTherapistComponent },
    {path: 'patient-progress', component: PatientProgressComponent},
    {path: 'chat', component: ChatComponent},
    {path: 'edit-progress', component: EditProgressComponent}
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    exports: [
    ],
})
export class AppRoutingModule { }
